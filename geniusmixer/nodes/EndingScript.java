package scripts.geniusmixer.nodes;

import org.tribot.api.General;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Login;
import scripts.geniusmixer.data.Node;
import scripts.geniusmixer.data.Vars;

public class EndingScript extends Node {
    @Override
    public boolean validate() {
        return Banking.isBankScreenOpen() && (Banking.find(Vars.firstMaterial).length == 0 ||
                Banking.find(Vars.secondMaterial).length == 0);
    }

    @Override
    public void execute() {

        Login.logout();
        General.sleep(1500, 3500);
        Vars.shouldRun = false;


    }
}
