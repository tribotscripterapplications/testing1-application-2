package scripts.geniusmixer.nodes;

import org.tribot.api.General;
import org.tribot.api2007.Banking;
import scripts.api.GeniuSWait;
import scripts.geniusmixer.data.Node;
import scripts.geniusmixer.data.Utility;
import scripts.geniusmixer.data.Vars;

import java.util.function.BooleanSupplier;

public class Withdraw extends Node {

    @Override
    public boolean validate() {
        return Banking.isBankScreenOpen() && Utility.bankHasFirstMaterial() && Utility.hasSecondMaterial() &&
                (!Utility.hasFirstMaterial() || !Utility.hasSecondMaterial());
    }

    @Override
    public void execute() {

        if (!(Banking.getWithdrawQuantity() == Banking.WITHDRAW_QUANTITY.WITHDRAW_X)) {
            if (Banking.setWithdrawQuantity(Banking.WITHDRAW_QUANTITY.WITHDRAW_X)) {
                GeniuSWait.waitTill((() -> Banking.getWithdrawQuantity().equals(Banking.WITHDRAW_QUANTITY.WITHDRAW_X)));
            }
        }

        if (!Utility.hasFirstMaterial()) {
            if (Banking.withdraw(14, Vars.firstMaterial)) {
                GeniuSWait.waitTill((Utility::hasFirstMaterial));
            }

        }

        if (!Utility.hasSecondMaterial()) {
            if (Banking.withdraw(14, Vars.secondMaterial)) {
                GeniuSWait.waitTill((Utility::hasSecondMaterial));

            }

        }

        if (Utility.hasFirstMaterial() && Utility.hasSecondMaterial()) {
            if (Banking.close()) {
                GeniuSWait.waitTill((() -> !Banking.isBankScreenOpen()));
            }
        } else {
            Banking.depositAll();
            General.sleep(500, 1000);
        }
    }
}
