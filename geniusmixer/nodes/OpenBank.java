package scripts.geniusmixer.nodes;

import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import scripts.api.GeniuSWait;
import scripts.geniusmixer.data.Node;
import scripts.geniusmixer.data.Utility;
import scripts.geniusmixer.data.Vars;

import java.util.function.BooleanSupplier;

public class OpenBank extends Node {
    @Override
    public boolean validate() {
        return !Banking.isBankScreenOpen() && (!Utility.hasFirstMaterial() || !Utility.hasSecondMaterial());
    }

    @Override
    public void execute() {

        if (!Banking.isBankScreenOpen()) {
            if (Banking.openBank()) {
                GeniuSWait.waitTill((Banking::isBankScreenOpen));
            }
        }

        if (Banking.isBankScreenOpen()) {
            if (Inventory.getAll().length > 0) {
                Banking.depositAll();
                GeniuSWait.waitTill((() -> Inventory.getAll().length == 0));
            }
        }
    }
}
