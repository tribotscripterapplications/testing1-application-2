package scripts.geniusmixer.nodes;

import org.tribot.api.General;
import org.tribot.api.input.Keyboard;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSItem;
import scripts.api.GeniuSWait;
import scripts.api.GeniuSTabing;
import scripts.geniusmixer.data.Constants;
import scripts.geniusmixer.data.Node;
import scripts.geniusmixer.data.Utility;
import scripts.geniusmixer.data.Vars;

import java.util.function.BooleanSupplier;

public class Combine extends Node {
    @Override
    public boolean validate() {
        return Utility.hasFirstMaterial() && Utility.hasSecondMaterial();
    }

    @Override
    public void execute() {


        RSItem[] firstMaterial = Inventory.find(Vars.firstMaterial);
        RSItem[] secondMaterial = Inventory.find(Vars.secondMaterial);


        if (Banking.isBankScreenOpen()) {
            if (Banking.close()) {
                GeniuSWait.waitTill((() -> !Banking.isBankScreenOpen()));
            }
        }

        GeniuSTabing.checkTab(GameTab.TABS.INVENTORY);

        if (firstMaterial.length > 0 && secondMaterial.length > 0) {
            if (Game.getItemSelectionState() == 0) {
                if (secondMaterial[0].click("Use")) {
                    General.sleep(500, 900);
                }
            }

            if (Game.getItemSelectionState() == 1) {
                if (firstMaterial[0].click("Use")) {
                    General.sleep(500, 1000);
                    GeniuSWait.waitTill((() -> Interfaces.isInterfaceValid(Constants.COMBINE_INTERFACE)));
                }
            }
        }

        RSInterface chat = Interfaces.get(162, 58);

        if (Interfaces.isInterfaceSubstantiated(Constants.COMBINE_INTERFACE)) {
            General.sleep(500, 1000);
            Keyboard.typeString(Vars.interfaces);

            BooleanSupplier supplier = () -> !Utility.hasFirstMaterial() || !Utility.hasSecondMaterial() || !Interfaces.isInterfaceSubstantiated(chat);
            GeniuSWait.waitTillLong(supplier);
            Vars.itemMadeCount += Utility.resultCount();
        }
    }
}
