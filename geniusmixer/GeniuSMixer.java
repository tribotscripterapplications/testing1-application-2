package scripts.geniusmixer;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Login;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Ending;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;
import scripts.api.AntiBan;
import scripts.geniusmixer.data.Constants;
import scripts.geniusmixer.data.Node;
import scripts.geniusmixer.data.Vars;
import scripts.geniusmixer.gui.GeniuSMixerGui;
import scripts.geniusmixer.nodes.*;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

@ScriptManifest(authors = {"GeniuSKilleR"}, category = "MoneyMaking", name = "GeniuS-Mixer", description = " ", version = (1))

public class GeniuSMixer extends Script implements Painting, Starting {


    private final List<Node> Mixer = Arrays.asList(new OpenBank(), new Withdraw(), new Combine(), new EndingScript());

    private GeniuSMixerGui gui;


    @Override
    public void onStart() {

        gui = new GeniuSMixerGui(this);
        gui.setVisible(true);

        while (gui.isVisible()) {
            sleep(100);
        }
        sleep(500);

        Vars.firstMaterial = gui.getFirstItem();
        Vars.secondMaterial = gui.getSecondMaterial();
        Vars.result = gui.getProduct();
        Vars.interfaces = gui.getInterface();

    }


    @Override
    public void run() {
        loop(Mixer);
    }


    private void loop(List<Node> nodes) {
        while (Vars.shouldRun) {
            for (final Node node : nodes) {
                if (node.validate()) {
                    node.execute();
                    sleep(General.random(20, 40));

                }
            }
        }
    }


    @Override
    public void onPaint(Graphics g1) {

        if (!(Login.getLoginState() == Login.STATE.INGAME) && Vars.timer.isRunning()) { // Used to pause timer at breaks
            Vars.timer.pause();
        }

        if ((Login.getLoginState() == Login.STATE.INGAME) && !Vars.timer.isRunning()) { // Used to resume timer after breaks
            Vars.timer.resume();
        }

        double multiplier = (double) Vars.timer.getElapsedTime() / 3600000.0D;
        int itemPerHour = (int) ((double) (Vars.itemMadeCount) / multiplier);


        Graphics2D g = (Graphics2D) g1;
        g.setColor(new Color(0, 0, 0, 180));
        g.fillRect(0, 0, 230, 120);
        g.setColor(new Color(255, 255, 255));
        g.drawRect(0, 0, 230, 120);
        g.setFont(Constants.FONT);
        g.setColor(Constants.COLOR);
        g.drawString("Run Time : " + Timing.msToString(this.getRunningTime()), 10, 20);
        g.drawString("Item Count = " + Vars.itemMadeCount, 10, 45);
        g.drawString("Item / Hour : " + itemPerHour, 10, 70);

    }
}