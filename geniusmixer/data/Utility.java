package scripts.geniusmixer.data;

import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;

public class Utility {

    public static boolean hasFirstMaterial() {
        return Inventory.getCount(Vars.firstMaterial) > 0;

    }


    public static boolean hasSecondMaterial() {

        return Inventory.getCount(Vars.secondMaterial) > 0;
    }

    public static boolean bankHasFirstMaterial() {
        return Banking.find(Vars.firstMaterial).length > 0;

    }


    public static boolean bankHasSecondMaterial() {

        return Banking.find(Vars.secondMaterial).length > 0;

    }

    public static int resultCount() {

        return Inventory.getCount(Vars.result);
    }


}
