package scripts.farming;

import org.tribot.api.General;
import org.tribot.api2007.Login;
import org.tribot.api2007.Skills;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Arguments;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;
import scripts.farming.nodes.Node;
import scripts.farming.nodes.progressive.*;
import scripts.farming.data.Vars;
import scripts.dax_api.api_lib.models.DaxCredentials;


import java.awt.*;
import java.util.*;
import java.util.List;

@ScriptManifest(authors = {"GeniuSKilleR"}, category = "Farming", name = "GeniuS-Farming", description = "Still In Beta xx", version = (5))

public class Farming extends Script implements Arguments, Starting, Painting {

    private final List<Node> progress = Arrays.asList(new BankingItems(), new TeleportingToHouse(), new BuildingPlant(), new FailSafeNoRingOfD(), new CheckingLevel());

    @Override
    public void passArguments(HashMap<String, String> hashMap) {
        String clientSelect = hashMap.get("custom_input");
        String clientStarter = hashMap.get("autostart");
        String input = clientStarter != null ? clientStarter : clientSelect;
        println("We Are Doing " + input);

        if (input.contains("PROG")) {
            Vars.isProgressive = true;
        }


        @Override
        public void run () {
            if (Vars.isProgressive) {
                loop(Progress);
            }
        }
    }

    private void loop(List<Node> nodes) {
        while (Vars.shouldRun) {
            for (final Node node : nodes) {
                if (node.validate()) {
                    node.execute();
                    sleep(General.random(20, 40));

                }
            }
        }
    }


    @Override
    public void onStart() {
        sleep(500);

        scripts.dax_api.api_lib.DaxWalker.setCredentials(new scripts.dax_api.api_lib.models.DaxCredentialsProvider() {
            @Override
            public DaxCredentials getDaxCredentials() {
                return new scripts.dax_api.api_lib.models.DaxCredentials("sub_DPjXXzL5DeSiPf", "PUBLIC-KEY");
            }
        });


    }


    @Override
    public void onPaint(Graphics g1) {

        double multiplier = (double) this.getRunningTime() / 3600000.0D;
        int xpPerHour = (int) ((double) (Skills.getXP(Skills.SKILLS.FARMING) - Vars.startXp) / multiplier);

        Graphics2D g = (Graphics2D) g1;
        g.setColor(new Color(0, 0, 0, 180));
        g.fillRect(0, 0, 230, 120);
        g.setColor(new Color(255, 255, 255));
        g.drawRect(0, 0, 230, 120);
        g.setFont(Vars.FONT);
        g.setColor(Vars.COLOR);
        g.drawString("Run Time : " + this.getRunningTime(), 10, 20);
        g.drawString("Current Level : " + Skills.getActualLevel(Skills.SKILLS.FARMING), 10, 45);
        g.drawString("XP : " + (Skills.getXP(Skills.SKILLS.FARMING) - Vars.startXp) + " (" + xpPerHour + ")", 50, 75);
    }
