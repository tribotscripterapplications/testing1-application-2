package scripts.farming.nodes.progressive;

import org.tribot.api.General;
import org.tribot.api2007.*;
import org.tribot.api2007.ext.Filters;
import org.tribot.api2007.types.*;
import scripts.farming.Farming;
import scripts.farming.data.Constants;
import scripts.farming.data.Utility;
import scripts.farming.nodes.Node;
import scripts.farming.data.Vars;
import scripts.api.GeniuSWait;
import scripts.api.GeniuSTabing;

import java.util.function.BooleanSupplier;

public class TeleportingToHouse extends Node {


    @Override
    public boolean validate() {

        RSObject[] door = Objects.find(15, "Door hotspot");

        return Utility.plantCount() > 0 && Utility.teleportToHouseCount() > 0 && Utility.wateringCanCount() > 0
                && door.length == 0;
    }

    @Override
    public void execute() {
        Vars.status = "Teleporting to house";


        if (Banking.isBankScreenOpen()) {
            if (Banking.close()) {
                GeniuSWait.waitTill((() -> !Banking.isBankScreenOpen()));
                General.sleep(General.randomSD(500, 1000, 800, 65));
            }

            GeniuSTabing.checkTab(GameTab.TABS.INVENTORY);

            RSItem[] houseTeleport = Inventory.find(Constants.TELEPORT_TO_HOUSE);
            RSObject[] portal = Objects.find(10, Filters.Objects.nameEquals("Portal").and(Filters.Objects.actionsContains("Lock")));


            if (houseTeleport.length > 0 && portal.length == 0) {
                if (houseTeleport[0].click("Break")) {
                    GeniuSWait.waitTill((() -> !Constants.CASTLE_WAR.contains(Player.getRSPlayer())));
                }

                if (!Constants.CASTLE_WAR.contains(Player.getRSPlayer())) {
                    GeniuSTabing.checkTab(GameTab.TABS.OPTIONS);
                }

                RSInterface house = Interfaces.get(261, 101);

                if (Interfaces.isInterfaceSubstantiated(Constants.HOUSE_TAB_INTERFACE)) {
                    if (house.click("View House Options")) {
                        GeniuSWait.waitTill((() -> Interfaces.isInterfaceSubstantiated(Constants.HOUSE_OPTIONS_INTERFACE)));
                    }

                    RSInterface buildingMode = Interfaces.get(370, 5);

                    if (Interfaces.isInterfaceValid(Constants.HOUSE_OPTIONS_INTERFACE)) {
                        if (buildingMode.click("on")) {
                            RSObject[] door = Objects.find(15, "Door hotspot");
                            GeniuSWait.waitTill((() -> door.length > 0));
                            General.sleep(General.randomSD(900, 2000, 1200, 65));
                        }
                    }
                }
            }
        }
    }
}


