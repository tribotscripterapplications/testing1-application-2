package scripts.farming.nodes.progressive;

import org.tribot.api.General;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSItem;
import scripts.farming.Farming;
import scripts.farming.data.Constants;
import scripts.farming.data.Utility;
import scripts.farming.data.Vars;
import scripts.farming.nodes.Node;
import scripts.api.GeniuSWait;
import scripts.api.GeniuSBanking;
import scripts.api.GeniuSTabing;
import scripts.dax_api.api_lib.models.RunescapeBank;

import java.util.function.BooleanSupplier;

public class BankingItems extends Node {


    @Override
    public boolean validate() {
        return (Utility.plantCount() == 0 || Utility.teleportToHouseCount() == 0 || Utility.emptyCanCount() >= 3)
                && (Utility.duelingEquipmentCount() > 0 || Constants.CASTLE_WAR.contains(Player.getRSPlayer()));
    }

    @Override
    public void execute() {

        Vars.status = "Banking";
        Utility.checkSupplies();

        RSItem[] ringOfDuelingEquipped = Equipment.find(Constants.RING_OF_DUELING);

        if (!Banking.isInBank() && !Constants.CASTLE_WAR.contains(Player.getRSPlayer())) {
            if (ringOfDuelingEquipped.length > 0) {
                GeniuSTabing.checkTab(GameTab.TABS.EQUIPMENT);

                if (ringOfDuelingEquipped[0].click("Castle Wars")) {
                    GeniuSWait.waitTill((() -> Constants.CASTLE_WAR.contains(Player.getRSPlayer())));
                }
            } else {


                GeniuSBanking.getItemFromBank(Constants.RING_OF_DUELING_8, 1);

                RSItem[] InventoryRingOfDueling = Inventory.find(Constants.RING_OF_DUELING_8);
                if (InventoryRingOfDueling.length > 0) {
                    GeniuSTabing.checkTab(GameTab.TABS.INVENTORY);

                    if (InventoryRingOfDueling[0].click("Wear")) {
                        BooleanSupplier supplier = () -> Inventory.getCount(Constants.RING_OF_DUELING_8) == 0;
                        GeniuSWait.waitTill(supplier);
                    }

                }
            }
        }

        if (!Banking.isInBank() && !Banking.isBankScreenOpen()) {
            GeniuSBanking.walkAndOpenBank(RunescapeBank.CASTLE_WARS);

        } else if (Banking.isBankScreenOpen()) {


            if (Utility.emptyCanCount() > 0) {
                Banking.depositAllExcept(Constants.TELEPORT_TO_HOUSE);
                GeniuSWait.waitTill((() -> Inventory.getAll().length == 0 || Inventory.getAll().length == 1));

            } else {

                if (!Banking.getWithdrawQuantity().equals(Banking.WITHDRAW_QUANTITY.WITHDRAW_ALL)) {
                    Banking.setWithdrawQuantity(Banking.WITHDRAW_QUANTITY.WITHDRAW_ALL);
                    General.sleep(500, 1000);
                }


                if (Utility.teleportToHouseCount() > 0) {
                    if (Utility.wateringCanCount() == 0) {
                        GeniuSBanking.getItemFromBank(Constants.WATERING_CAN, 3);
                    }

                    if (Utility.plantCount() == 0) {
                        GeniuSBanking.getItemFromBank(Constants.BAGGED_PLANT1, 0);
                    } else if (Constants.BAGGED_PLANT1 > 0) {
                        GeniuSBanking.closeBank();
                    }


                } else if (Utility.teleportToHouseCount() == 0) {
                    GeniuSBanking.getItemFromBank(Constants.TELEPORT_TO_HOUSE, 0);
                }


            }
        } else {
            if (Banking.openBank()) {
                General.sleep(500, 1500);
            }
        }
    }
}



