package scripts.farming.nodes.progressive;

import org.tribot.api.General;
import org.tribot.api.input.Keyboard;
import org.tribot.api2007.*;
import org.tribot.api2007.ext.Filters;
import org.tribot.api2007.types.RSObject;
import scripts.api.GeniuSTabing;
import scripts.farming.data.Constants;
import scripts.farming.data.Utility;
import scripts.farming.nodes.Node;
import scripts.farming.data.Vars;
import scripts.api.GeniuSWait;

import java.util.function.BooleanSupplier;


public class BuildingPlant extends Node {


    @Override
    public boolean validate() {
        return !Constants.CASTLE_WAR.contains(Player.getRSPlayer()) && Utility.emptyCanCount() < 3;
    }

    @Override
    public void execute() {


        if (Vars.startXp == 0) {
            Vars.startXp = Skills.getXP(Skills.SKILLS.FARMING);
        }

        GeniuSTabing.checkTab(GameTab.TABS.INVENTORY);


        RSObject[] plant = Objects.findNearest(10, Filters.Objects.nameEquals("Plant").or(Filters.Objects.idEquals(Constants.PLANT)));
        RSObject[] plantSpace = Objects.findNearest(10, Filters.Objects.nameEquals("Small Plant Space 1").or(Filters.Objects.idEquals(Constants.PLANT_SPACE)));


        if (plant.length > 0) {
            if (plant[0].click("Remove")) {
                GeniuSWait.waitTill((() -> (Interfaces.isInterfaceSubstantiated(Constants.CHAT_OPTION_INTERFACE))
                        || (Interfaces.isInterfaceSubstantiated(Constants.LEVEL_UP_INTERFACE))));
                General.sleep(General.randomSD(600, 1800, 900, 65));
            }


            if (Interfaces.isInterfaceSubstantiated(Constants.CHAT_OPTION_INTERFACE)) {
                Keyboard.typeString("1");
                BooleanSupplier supplier = () -> plantSpace.length > 1 || Player.getAnimation() == -1;
                GeniuSWait.waitTill(supplier);
                General.sleep(General.randomSD(600, 1800, 900, 65));
            }
        }

        if (plantSpace.length > 0) {
            if (plantSpace[0].click("Build")) {
                GeniuSWait.waitTill((() -> Interfaces.isInterfaceSubstantiated(Constants.BUILDING_INTERFACE)));
                General.sleep(General.randomSD(500, 1000, 800, 65));
            }


            if (Interfaces.isInterfaceSubstantiated(Constants.BUILDING_INTERFACE)) {
                Keyboard.typeString("1");
                GeniuSWait.waitTill((() -> !Interfaces.isInterfaceValid(Constants.BUILDING_INTERFACE)));
                General.sleep(General.randomSD(800, 2500, 1000, 65));
            }
        }
    }
}
