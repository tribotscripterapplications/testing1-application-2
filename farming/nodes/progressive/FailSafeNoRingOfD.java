package scripts.farming.nodes.progressive;

import org.tribot.api2007.*;
import org.tribot.api2007.ext.Filters;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSObject;
import scripts.api.GeniuSBanking;
import scripts.farming.Farming;
import scripts.farming.data.Constants;
import scripts.farming.data.Utility;
import scripts.farming.nodes.Node;
import scripts.farming.data.Vars;
import scripts.api.GeniuSWait;

import java.util.function.BooleanSupplier;


public class FailSafeNoRingOfD extends Node {

    @Override
    public boolean validate() {
        RSObject[] portal = Objects.find(10, Filters.Objects.nameEquals("Portal").and(Filters.Objects.actionsContains("Lock")));


        return portal.length > 0;
    }

    @Override
    public void execute() {
        Vars.status = "Ring Fail Safe";


        if ((Utility.plantCount() == 0 || Utility.teleportToHouseCount() == 0 || Utility.emptyCanCount() == 3)
                && Utility.duelingEquipmentCount() == 0) {
            GeniuSBanking.walkAndOpenNearestBank();
        } else {


            RSObject[] portal = Objects.find(10, Filters.Objects.nameEquals("Portal").and(Filters.Objects.actionsContains("Lock")));

            if (portal.length > 0) {
                if (portal[0].click("Enter")) {
                    BooleanSupplier supplier = () -> false;
                    GeniuSWait.waitTill(supplier);
                }
            }
        }
    }
}
