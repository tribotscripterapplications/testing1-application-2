package scripts.farming.nodes.progressive;

import org.tribot.api.General;
import org.tribot.api2007.Skills;
import scripts.farming.Farming;
import scripts.farming.nodes.Node;
import scripts.farming.data.Vars;

public class CheckingLevel extends Node {
    @Override
    public boolean validate() {
        return Skills.getActualLevel(Skills.SKILLS.FARMING) >= 30;
    }

    @Override
    public void execute() {
        Vars.status = "Stopping";
        General.println("We Are Level 30+  ");
        Vars.shouldRun = false;
    }
}
