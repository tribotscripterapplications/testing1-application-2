package scripts.farming.nodes;


public abstract class Node {

    public abstract boolean validate();


    public abstract void execute();


}