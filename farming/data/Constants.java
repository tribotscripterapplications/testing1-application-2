package scripts.farming.data;

import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import java.awt.*;

public class Constants {
    public static Color COLOR = new Color(248, 250, 255);
    public static Font FONT = new Font("Arial", 0, 17);

    public static int BAGGED_PLANT1 = 8431;
    public static int TELEPORT_TO_HOUSE = 8013;
    public static int EMPTY_WATERING_CAN = 5331;
    public static int WATERING_CAN = 5340;
    public static int RING_OF_DUELING_8 = 2552;
    public static int PLANT_SPACE = 15366;
    public static int PLANT = 5134;
    public static int CHAT_OPTION_INTERFACE = 219;
    public static int LEVEL_UP_INTERFACE = 233;
    public static int BUILDING_INTERFACE = 458;
    public static int HOUSE_TAB_INTERFACE = 458;
    public static int HOUSE_OPTIONS_INTERFACE = 458;

    public static int[] RING_OF_DUELING = {2552, 2554, 2556, 2558, 2560, 2562, 2564, 2566};

    public static RSArea CASTLE_WAR = new RSArea(
            new RSTile(2425, 3071, 0),
            new RSTile(2460, 3109, 0)
    );


}
