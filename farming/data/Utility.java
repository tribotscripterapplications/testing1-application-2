package scripts.farming.data;

import org.tribot.api.General;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Equipment;
import org.tribot.api2007.Inventory;

public class Utility {


    public static int duelingEquipmentCount() {
        return Equipment.getCount(Constants.RING_OF_DUELING);

    }

    public static int wateringCanCount() {
        return Inventory.getCount(Constants.WATERING_CAN);

    }

    public static int plantCount() {
        return Inventory.getCount(Constants.BAGGED_PLANT1);

    }

    public static int teleportToHouseCount() {
        return Inventory.getCount(Constants.TELEPORT_TO_HOUSE);

    }

    public static int emptyCanCount() {
        return Inventory.getCount(Constants.EMPTY_WATERING_CAN);

    }


    public static void checkSupplies() {
        if (Banking.find(Constants.WATERING_CAN).length == 0 && Inventory.getCount(Constants.WATERING_CAN) == 0) {
            General.println("Out Of Watering Can ");
            Vars.shouldRun = false;
        }

        if (Banking.find(Utility.plantCount()).length == 0 && Inventory.getCount(Utility.plantCount()) == 0) {
            General.println("Out Of Bagged Plant 1 ");
            Vars.shouldRun = false;
        }

        if (Banking.find(Constants.TELEPORT_TO_HOUSE).length == 0 && Inventory.getCount(Constants.TELEPORT_TO_HOUSE) == 0) {
            General.println("Out Of House Teleport  ");
            Vars.shouldRun = false;
        }

        if (Banking.find(Constants.RING_OF_DUELING).length == 0 && Equipment.getCount(Constants.RING_OF_DUELING) == 0) {
            General.println("Out Of Ring of Dueling ");
            Vars.shouldRun = false;
        }
    }

}
