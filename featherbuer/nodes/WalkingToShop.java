package scripts.featherbuer.nodes;

import org.tribot.api.General;
import org.tribot.api2007.Player;
import scripts.api.GeniuSWait;
import scripts.featherbuer.data.Constants;
import scripts.featherbuer.data.Node;

import java.util.function.BooleanSupplier;

public class WalkingToShop extends Node {


    @Override
    public boolean validate() {
        return !Constants.SHOP_AREA.contains(Player.getRSPlayer());
    }

    @Override
    public void execute() {

        scripts.dax_api.api_lib.DaxWalker.walkTo(Constants.MERCHANT.getRandomTile());
       GeniuSWait.waitTill((()->Constants.SHOP_AREA.contains(Player.getRSPlayer())));
        General.randomSD(500, 1500, 800, 80);
    }
}
