package scripts.featherbuer.nodes;

import org.tribot.api.General;
import org.tribot.api2007.GameTab;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSItem;
import scripts.api.GeniuSWait;
import scripts.api.GeniuSTabing;
import scripts.featherbuer.data.Constants;
import scripts.featherbuer.data.Node;
import scripts.featherbuer.data.Vars;

import java.util.function.BooleanSupplier;

public class OpeningFeathersAll extends Node {


    @Override
    public boolean validate() {
        return Inventory.getCount(Constants.FEATHER_PACK) > 0;
    }

    @Override
    public void execute() {
        RSItem[] pack = Inventory.find(Constants.FEATHER_PACK);
        GeniuSTabing.checkTab(GameTab.TABS.INVENTORY);

        RSInterface close = Interfaces.get(300, 1, 11);

        if (pack.length > 0) {
            if (Interfaces.isInterfaceSubstantiated(close)) {
                if (close.click("Close")) {
                    General.sleep(900, 1500);
                }
            }
            Vars.feathercount += pack.length * 100;

            for (RSItem rsItem : Inventory.find(Constants.FEATHER_PACK)) {
                rsItem.click();
            }
            GeniuSWait.waitTill((() -> Inventory.getCount(Constants.FEATHER_PACK) == 0));

        }

    }
}
