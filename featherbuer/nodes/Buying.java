package scripts.featherbuer.nodes;

import org.tribot.api.General;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSInterface;
import scripts.api.GeniuSWait;
import scripts.api.GeniuSInterfaces;
import scripts.featherbuer.data.Constants;
import scripts.featherbuer.data.Node;

import java.util.function.BooleanSupplier;

public class Buying extends Node {
    @Override
    public boolean validate() {
        return Inventory.getCount(Constants.FEATHER_PACK) == 0 && Interfaces.isInterfaceSubstantiated(Constants.SHOP_INTERFACE)
                && (Interfaces.get(300, 16, 9) != null && Interfaces.get(300, 16, 9).getComponentStack() > 90);// checking the count of feathers at shop must be more than 90
    }

    @Override
    public void execute() {

        RSInterface feather = Interfaces.get(300, 16, 9);
        RSInterface close = Interfaces.get(300, 1, 11);

        if (Interfaces.isInterfaceSubstantiated(feather)) {

            if (feather.click("Buy 50")) {
                GeniuSWait.waitTill((Inventory::isFull));
                General.sleep(500, 1800);

                if (Interfaces.isInterfaceSubstantiated(close)) {
                    if (close.click("Close")) {
                        GeniuSWait.waitTill((close::isHidden));
                    }
                }
            }
        }
    }
}
