package scripts.featherbuer.nodes;

import org.tribot.api.General;
import org.tribot.api2007.*;
import org.tribot.api2007.types.RSNPC;
import scripts.api.GeniuSWait;
import scripts.featherbuer.data.Constants;
import scripts.featherbuer.data.Node;

import java.util.function.BooleanSupplier;

public class Trading extends Node {
    @Override
    public boolean validate() {
        return Inventory.getCount(Constants.FEATHER_PACK) == 0 && Interfaces.isInterfaceSubstantiated(Constants.SHOP_INTERFACE);
    }

    @Override
    public void execute() {

        RSNPC[] merchant = NPCs.findNearest("Gerrant");

        if (merchant.length > 0) {
            if (PathFinding.canReach(merchant[0].getPosition(),false)) {
                if (merchant[0].isClickable() && merchant[0].isOnScreen()) {
                    if (merchant[0].click("Trade")) {
                        GeniuSWait.waitTill((() -> !Player.isMoving() || Interfaces.isInterfaceValid(Constants.SHOP_INTERFACE)));
                    }

                } else {
                    Walking.clickTileMM(merchant[0].getPosition(), 1);
                    General.randomSD(1000, 3000, 1500, 60);
                }
            }else{
                scripts.dax_api.api_lib.DaxWalker.walkTo(merchant[0].getPosition());
                GeniuSWait.waitTill((()->PathFinding.canReach(merchant[0].getPosition(),false)));
                General.randomSD(500, 1500, 800, 80);

            }
        }

    }
}
