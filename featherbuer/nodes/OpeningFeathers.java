package scripts.featherbuer.nodes;

import org.tribot.api.General;
import org.tribot.api2007.GameTab;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSItem;
import scripts.api.GeniuSWait;
import scripts.api.GeniuSTabing;
import scripts.featherbuer.data.Constants;
import scripts.featherbuer.data.Node;
import scripts.featherbuer.data.Vars;

import java.util.function.BooleanSupplier;

public class OpeningFeathers extends Node {


    @Override
    public boolean validate() {
        return Inventory.getCount(Constants.FEATHER_PACK) > 0;
    }

    @Override
    public void execute() {
        RSItem[] pack = Inventory.find(Constants.FEATHER_PACK);
        RSInterface close = Interfaces.get(300, 1, 11);
        GeniuSTabing.checkTab(GameTab.TABS.INVENTORY);

        if (pack.length > 0) {
            if (Interfaces.isInterfaceSubstantiated(close)) {
                if (close.click("Close")) {
                    General.sleep(900, 1200);
                }
            }

            if (pack[0].click("Open")) {
                Vars.feathercount += pack.length * 100;
                GeniuSWait.waitTill((() -> Inventory.getCount(Constants.FEATHER_PACK) == 0));
                General.sleep(General.randomSD(2500, 3500, 2000, 60));
            }
        }
    }
}
