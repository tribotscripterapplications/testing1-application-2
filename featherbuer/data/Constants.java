package scripts.featherbuer.data;

import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import java.awt.*;

public class Constants {

    public static Color COLOR = new Color(248, 250, 255);
    public static Font FONT = new Font("Arial", Font.PLAIN, 17);

    public static int FEATHER_PACK = 11881;
    public static int SHOP_INTERFACE = 300;

    public static RSArea SHOP_AREA = new RSArea(
            new RSTile(3009, 3230, 0),
            new RSTile(3018, 3219, 0)
    );

    public static RSArea MERCHANT = new RSArea(
            new RSTile(3012, 3227, 0),
            new RSTile(3016, 3222, 0)
    );


}
