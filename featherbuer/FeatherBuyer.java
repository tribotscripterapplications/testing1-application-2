package scripts.featherbuer;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.*;
import scripts.featherbuer.data.Constants;
import scripts.featherbuer.nodes.*;

import scripts.featherbuer.data.Node;
import scripts.featherbuer.data.Vars;
import scripts.dax_api.api_lib.models.DaxCredentials;


import java.awt.*;
import java.util.*;
import java.util.List;


@ScriptManifest(authors = {"GeniuSKilleR"}, category = "Money-Making", name = "FeatherBuyer", description = "Join Discord For Instructions ", version = (2))

public class FeatherBuyer extends Script implements Arguments, MessageListening07, Painting, Starting {

    public static final List<Node> all = Arrays.asList(new Trading(), new Buying(), new OpeningFeathersAll(), new WalkingToShop());

    public static final List<Node> onlyOne = Arrays.asList(new Trading(), new Buying(), new OpeningFeathers(), new WalkingToShop());


    @Override
    public void passArguments(HashMap<String, String> hashMap) {
        String clientSelect = hashMap.get("custom_input");
        String clientStarter = hashMap.get("autostart");
        String input = clientStarter != null ? clientStarter : clientSelect;
        if (input.equals("ALL")) {
            Vars.all = true;
        }

        if (input.equals("ONE")) {
            Vars.onlyOne = true;
        }
    }

    @Override
    public void onStart() {
        sleep(500);

        scripts.dax_api.api_lib.DaxWalker.setCredentials(new scripts.dax_api.api_lib.models.DaxCredentialsProvider() {
            @Override
            public DaxCredentials getDaxCredentials() {
                return new scripts.dax_api.api_lib.models.DaxCredentials("sub_DPjXXzL5DeSiPf", "PUBLIC-KEY");
            }
        });
    }


    @Override
    public void run() {

        if (Vars.onlyOne) {
            loop(onlyOne);
        }

        if (Vars.all) {
            loop(all);
        }
    }

    public static void loop(List<Node> nodes) {
        while (Vars.hasGold()) {
            for (final Node node : nodes) {
                if (node.validate()) {
                    node.execute();
                    General.sleep(20, 40);
                }
            }
        }
    }


    @Override
    public void onPaint(Graphics g1) {
        double multiplier = (double) this.getRunningTime() / 3600000.0D;
        int FeathersPerHour = (int) ((double) (Vars.feathercount) / multiplier);
        Graphics2D g = (Graphics2D) g1;
        g.setColor(new Color(0, 0, 0, 180));
        g.fillRect(0, 0, 230, 120);
        g.setColor(new Color(255, 255, 255));
        g.drawRect(0, 0, 230, 120);
        g.setFont(Constants.FONT);
        g.setColor(Constants.COLOR);
        g.drawString("Run Time : " + Timing.msToString(this.getRunningTime()), 10, 20);
        g.drawString("Feathers Count : " + Vars.feathercount, 10, 45);
        g.drawString("Feathers / Hour : " + FeathersPerHour, 10, 70);

    }


}
